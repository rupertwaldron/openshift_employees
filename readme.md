Create the network
`docker network create employee-mysql`

List networks
`docker network create employee-mysql`

Create mysql container
`docker container run --name mysqldb --network employee-mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=bootdb -d mysql:latest`
a528adb96ccb53b3ffe68ca23c45f901346b701a89f795225f04d6e7f28f17dc

Check logs need first 2 characters from above
`docker container logs -f a5`

Check the database has been created correctly
`docker container exec -it a5 bash`
`root@a528adb96ccb:/# mysql -uroot -proot;`
`mysql> show databases;`


Update application.properties
`spring.datasource.url=jdbc:mysql://mysqldb/bootdb
spring.datasource.username=root
spring.datasource.password=root`

Create image after adding docker file
`docker image build -t employeedocker .`


Run the container
`docker container run --network employee-mysql --name employeedocker-container -p 8080:8080 -d employeedocker`
c6460f29eda77f39307598833ef69219d6b45087af8c5d28d8da930d464d3fda

Check the logs
`docker container logs -f c6`


## Update for OpenShift

### Image

Build the image

`docker build -t employee_docker:v1 .`

Run the image

`docker run -it -p 8080:8080 employee_docker:v1`


