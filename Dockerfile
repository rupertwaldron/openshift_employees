FROM openjdk:11

COPY ./build/libs/EmployeeDocker-0.0.1-SNAPSHOT.jar /java/EmployeeDocker-0.0.1-SNAPSHOT.jar

ENV HOME /java

RUN chgrp -R 0 /java && chmod -R g+rwX /java

EXPOSE 8080

LABEL io.openshift.expose-services 8080/http

USER 1001

CMD ["java","-jar","/java/EmployeeDocker-0.0.1-SNAPSHOT.jar"]
