package com.ruppyrup.employeedocker.repositories;

import com.ruppyrup.employeedocker.model.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@RequiredArgsConstructor
public class DataInitializer implements CommandLineRunner {
    private final EmployeeRepository repository;
    @Override
    public void run(String... args) throws Exception {
        repository.save(new Employee(null, "Bob"));
        repository.save(new Employee(null, "Tom"));
        repository.save(new Employee(null, "Dick"));
        repository.save(new Employee(null, "Harry"));
    }
}
