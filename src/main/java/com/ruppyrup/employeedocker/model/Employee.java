package com.ruppyrup.employeedocker.model;


import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EMPLOYEES")
public class Employee {
    @Column(name = "EMP_ID", nullable = false)
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long empid;

    @Column(name = "EMP_NAME", nullable = false)
    private String empName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Employee employee = (Employee) o;

        return empid != null && empid.equals(employee.empid);
    }

    @Override
    public int hashCode() {
        return 949447908;
    }
}
